package com.example.hejuso.examendiejercicio1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView texto_edit = findViewById(R.id.texto_edit);
        final LinearLayout layout_texto = findViewById(R.id.layout_texto);

        // FONDOS
        final RadioButton fondo_negro = findViewById(R.id.fondo_negro);
        final RadioButton fondo_verde = findViewById(R.id.fondo_verde);
        final RadioButton fondo_rojo = findViewById(R.id.fondo_rojo);

        // TEXTOS
        final RadioButton texto_blanco = findViewById(R.id.texto_blanco);
        final RadioButton texto_amarillo = findViewById(R.id.texto_amarillo);
        final RadioButton texto_azul = findViewById(R.id.texto_azul);

        // MOSTRAR TEXTO
        final CheckBox mostrar_texto = findViewById(R.id.mostrar_texto);

        // FONDO Negro
        fondo_negro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            layout_texto.setBackgroundColor(Color.BLACK);
            }
        });

        // FONDO Verde
        fondo_verde.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            layout_texto.setBackgroundColor(Color.GREEN);
            }
        });

        // FONDO Rojo
        fondo_rojo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            layout_texto.setBackgroundColor(Color.RED);
            }
        });

        // TEXTO Blanco
        texto_blanco.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                texto_edit.setTextColor(Color.WHITE);
            }
        });

        // TEXTO Amarillo
        texto_amarillo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                texto_edit.setTextColor(Color.YELLOW);
            }
        });

        // TEXTO Blanco
        texto_azul.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                texto_edit.setTextColor(Color.BLUE);
            }
        });

        // TEXTO Blanco
        texto_azul.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                texto_edit.setTextColor(Color.BLUE);
            }
        });

        mostrar_texto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            if(mostrar_texto.isChecked()){
                texto_edit.setVisibility(View.VISIBLE);
            }else{
                texto_edit.setVisibility(View.INVISIBLE);
            }
            }
        });

    }
}
